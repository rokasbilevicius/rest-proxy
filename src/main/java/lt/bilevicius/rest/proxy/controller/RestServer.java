package lt.bilevicius.rest.proxy.controller;

import javax.servlet.http.HttpServletRequest;
import lt.bilevicius.rest.proxy.model.Command;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestServer {

  @RequestMapping("/register")
  public Command register(@RequestParam(value = "id") String id,
                          @RequestParam(value = "content") String content,
                          HttpServletRequest request) {
    return new Command(id, request.getServletPath(), content);
  }

}
