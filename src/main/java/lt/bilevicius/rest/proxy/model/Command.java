package lt.bilevicius.rest.proxy.model;

public class Command {
  private final String id;
  private final String command;
  private final String content;


  /** Defines command message construct.
   * @param id received message guid
   * @param command endpoint which was used to send the contents
   * @param content message content for received command
   */
  public Command(String id, String command, String content) {
    this.id = id;
    this.command = command;
    this.content = content;
  }

  public String getId() {
    return this.id;
  }

  public String getCommand() {
    return this.command;
  }

  public String getContent() {
    return this.content;
  }

}
